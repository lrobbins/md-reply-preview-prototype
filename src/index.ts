import { App } from './app/app';
import { Root } from './lib/root';

window.addEventListener('load', () => {
    Root.create().attach(App);
})