import { Component } from './plain.element';

/**
 * Application Root
 * 
 * This attaches the root application component to the dom.
 * 
 * If no selector string is present then it attaches to the document body
 */
export class Root {
    private readonly domHook: Element; 

    /**
     * Ctor
     * @param selector - css selector string
     */
    private constructor(selector?: string) {
        this.domHook = selector ? 
            document.querySelector(selector) : document.body;
    }

    /**
     * Attach our component
     * @param component
     */
    public attach(component: new () => Component<HTMLElement>): void {
        const app = new component();

        this.domHook.appendChild(app.render());
    }

    /**
     * Initialise our root element
     * @param selector 
     */
    static create(selector?: string): Root {
        return new Root(selector);
    }
}