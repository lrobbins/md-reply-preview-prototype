/**
 * Element options
 */
export interface ElementOptions {
    tagName?: string;
    attributes?: Dictionary<string>;
    classList?: string[];
    dataset?: Dictionary<string>;
    events?: Dictionary<Function>;
    children?: Component<HTMLElement>[] | string;
}

/**
 * Component definition
 */
export interface Component<T> {
    render(): T;
}

/* Dictionary type for our key value pairs */
export interface Dictionary<T> {
    [key: string]: T;
}

/**
 * Plain element
 */
export class PlainElement implements Component<HTMLElement> {
    private readonly _element: HTMLElement;
    private _children: Component<HTMLElement>[];
    private readonly _tagName: string;
    private readonly _classList: string[];
    private readonly _dataSet: Dictionary<string>;
    private readonly _attributes: Dictionary<string>;
    private readonly _events: Dictionary<Function>;
    
    /**
     * Ctor
     *
     * @param options - element options
     */
    protected constructor(options?: ElementOptions) {
        options = options || {};

        this._children = [];
        this._tagName = options.tagName || 'div';
        this._classList =  options.classList || [];
        this._dataSet = options.dataset || {};
        this._attributes = options.attributes || {};
        this._events = options.events || {};
        this._element = document.createElement(this._tagName);

        if(options.children && typeof options.children === 'string') {
            this._element.innerHTML = options.children as string;
        } else if(options.children && options.children instanceof Array) {
            this._children = options && options.children as Component<HTMLElement>[] || new Array<Component<HTMLElement>>();
        }
    }
    
    //@ts-ignore
    set innerHTML(value: string) {
        this._element.innerHTML = value;
    }

    /**
     * Set our element attributes based on a simple object
     * @param attributes
     */
    setAttributes(attributes: Dictionary<string>): void {
        if(!attributes) return;

        Object.keys(attributes).forEach(key => { 
            this._attributes[key] = attributes[key] as string;
            
            /* 
                Some of the elements have filed as part of the element interface e.g. input with value attribute 
                Check whether the key exists and if so always prefer this over the using setAttribute.
            */
            if(this._element[key]) {
                this._element[key] = attributes[key];
            } else {
                this._element.setAttribute(key, attributes[key]);
            }
        });
    }
    
    /**
     * Adds a set of classes
     * @param classList 
     */
    addClassList(...classList: string[]): void {
        if(!classList) return;
        if(!classList.length) return;
        
        this._element.classList.add(...classList);
    }

    /**
     * Removes a set of classes
     * @param classList
     */
    removeClassList(...classList: string[]): void {
        if(!classList) return;
        if(!classList.length) return;
        
        this._element.classList.remove(...classList);
    }

    /**
     * Add a dataset to our element
     * @param dataSet 
     */
    addDataSet(dataSet: Dictionary<string>): void {
        if(!dataSet) return;

        Object
            .keys(dataSet)
            .forEach(key => { 
                this._dataSet[key] = dataSet[key];
                this._element.dataset[key] = dataSet[key];
            });
    }

    /**
     * remove dataset elements
     * @param keys 
     */
    removeDataSet(...keys: string[]): void {
        if(!keys) return;
        if(!keys.length) return;

        keys.forEach(key => delete this._dataSet[key]);
    }

    /**
     * Add a child component
     * @param child 
     */
    addChild(child: Component<HTMLElement>): void {
        if(!child) return;

        this._children.push(child);
    }

    /**
     * Remove a child component
     * @param child
     */
    removeChild(child: Component<HTMLElement>): void {
        if(!child) return;

        const index = this._children.indexOf(child);

        this._children = this._children.slice(index, 1);
    }

    /**
     * Build our final element
     */
    render(): HTMLElement {
        this._children.forEach(item =>
            this._element.appendChild(item.render())
        );

        if(this._classList.length) {
            this._element.classList.add(...this._classList);
        }
        
        Object
            .keys(this._attributes)
            .forEach(key => {
                if(this._element[key]) {
                    this._element[key] = this._attributes[key];
                } else {
                    this._element.setAttribute(key, this._attributes[key]);
                }
            });

        Object
            .keys(this._events)
            .forEach(
                //@ts-ignore
                (eventName) => this._element.addEventListener(eventName, (event) => this._events[eventName](event))
            );

        Object
            .keys(this._dataSet)
            .forEach(
                //@ts-ignore
                key => this._element.dataset[key] = this._dataSet[key]
            );

        return this._element;
    }

    static create(options?: ElementOptions): PlainElement {
        return new PlainElement(options);
    }
}