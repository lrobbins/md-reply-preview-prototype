import { Observer } from './observer';

/**
 * Subject implementation
 */
export class Subject<T> {
    private observers: Observer<T>[];
    private state: T;

    /**
     * Ctor
     */
    constructor() {
        this.observers = [];
    }

    /**
     * Add an observer
     * @param observer 
     */
    attach(observer: Observer<T>) {
        this.observers.push(observer);
    }

    /**
     * Removes an observer
     * @param observer 
     */
    detach(observer: Observer<T>) {
        const index = this.observers.indexOf(observer);

        if (index === -1) return;
        
        this.observers = this.observers.slice(index, 1);
    }

    /**
     * Notify our observers
     * @param data - data to notify about
     */
    notify(data: T) {
        this.observers.forEach(
            observer => observer.update(data)
        );
    }
}
