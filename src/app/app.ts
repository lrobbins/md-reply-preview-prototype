import { Component, PlainElement } from '../lib/plain.element';
import { ReplyForm } from './reply.form';

export class App implements Component<HTMLElement> {
    render(): HTMLElement {
        const app = PlainElement.create({ classList: ['app']});

        let header = PlainElement.create({
            children: [ 
                PlainElement.create({
                    tagName: 'h1',
                    children: 'Mark down message preview'
                }),
                PlainElement.create({
                    tagName: 'p',
                    children: 'Start typing some markdown text below:'
                })
            ]
        });

        let replyForm = new ReplyForm();


        app.addChild(header);
        app.addChild(replyForm);

        return app.render();
    }
}