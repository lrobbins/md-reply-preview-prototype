import { Component, PlainElement } from "../lib/plain.element";

export interface ReplyTextAreaOptions {
    maxlength?: number;
}

export class ReplyTextArea implements Component<HTMLElement> {
    private readonly textarea: PlainElement;

    constructor(textAreaOptions?: ReplyTextAreaOptions) {
        textAreaOptions = textAreaOptions || {};
        textAreaOptions.maxlength = textAreaOptions.maxlength | 2000;

        this.textarea = PlainElement.create({
            tagName: 'textarea',
            classList: ['reply-text-area'],
            attributes: {
                placeholder: 'Write your message here ...',
                name: 'reply-text-area',

            },
            dataset: {
                example: 'example-dataset-value'
            }
        }); 
    }

    empty(): void {
        this.textarea.setAttributes({ value: '' });
    }

    render(): HTMLElement {
        return this.textarea.render();
    }
}