import { Component } from "../lib/plain.element";
import { PlainElement } from "../lib/plain.element";
import { Subject } from "../lib/subject";
import { ReplyTextArea } from './reply.textarea';
import { ReplyPreview } from "./reply.preview";
import { CharacterCount } from "./character.count";

export class ReplyForm implements Component<HTMLElement> {
    private readonly form: PlainElement;
    private readonly subject: Subject<string>;

    constructor() {
        /* Create our form Element */
        this.form = PlainElement.create({
            tagName: 'form',
            classList: ['reply-form'],
            events: {
                keyup: (event: KeyboardEvent) => {
                    event.preventDefault();

                    const textarea = event.target as HTMLTextAreaElement;
            
                    this.subject.notify(textarea.value);            
                }
            }
        });

        this.subject = new Subject<string>();

        const preview = new ReplyPreview();
        const replyTextArea = new ReplyTextArea();
        const wordCount = new CharacterCount({ max: 2000 });

        /* Create our buttons */
        const resetButton = PlainElement.create({
            tagName: 'button',
            children: "reset",
            attributes: {
                type: 'reset'
            },
            events: {
                click: (event: MouseEvent) => {
                    event.preventDefault();

                    replyTextArea.empty();

                    this.subject.notify('');
                }
            }
        });

        const submitButton = PlainElement.create({
            tagName: 'button',
            children: 'submit',
            attributes: {
                type: 'submit'
            },
            events: {
                click: (event: MouseEvent) => {
                    alert("this is where we could submit the form");
                }
            }
        });

        /* add our sub comonents to the form */
        this.form.addChild(replyTextArea);
        this.form.addChild(wordCount);
        this.form.addChild(preview);        
        this.form.addChild(resetButton);
        this.form.addChild(submitButton);

        /* add our observers to our subject */
        this.subject.attach(preview);
        this.subject.attach(wordCount);
    }

    /* Render our form */
    render(): HTMLElement {
        return this.form.render();
    }
}