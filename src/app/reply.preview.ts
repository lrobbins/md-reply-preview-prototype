import { Observer } from "../lib/observer";
import { Component, PlainElement } from "../lib/plain.element";
import MarkdownIt = require('../../node_modules/markdown-it/dist/markdown-it.js');

export class ReplyPreview implements Observer<string>, Component<HTMLElement> {
    private readonly preview: PlainElement;
    private readonly md: MarkdownIt;

    constructor() {
        this.preview = PlainElement.create({ 
            classList: ['reply-preview'] 
        });

        //TODO: markdownit and system js really don't like one another, need to fix later
        this.md = new MarkdownIt.default();
    }

    update(data: string): void {
        this.preview.innerHTML = this.md.render(data);
    }

    render(): HTMLElement {
        return this.preview.render();
    }
}