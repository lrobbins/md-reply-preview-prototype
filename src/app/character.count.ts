import { Observer } from "../lib/observer";
import { Component, PlainElement } from "../lib/plain.element";

export interface CharacterCountOptions {
    max?: number;
}

export class CharacterCount implements Observer<string>, Component<HTMLElement> {
    private readonly _counter: PlainElement;
    private readonly _max: number;

    private _text: string = '';
    
    constructor(options?: CharacterCountOptions) {
        options = options || {};
        this._max = options.max || undefined;

        this._text = '';

        this._counter = PlainElement.create({ 
            classList: ['reply-word-count'],
            children: this.buildCounterText()
        });
    }

    private buildCounterText(): string {
        if(this._max) {
            return `${this._text.length} / ${this._max}`;
        } else {
            return `${this._text.length}`;
        }
    }

    update(text: string): void {
        /* Don't bother updating is nothing has changed */
        if(text === this._text) return; 

        this._text = text;

        this._counter.innerHTML = this.buildCounterText();
    }

    render(): HTMLElement {
        return this._counter.render();
    }
}