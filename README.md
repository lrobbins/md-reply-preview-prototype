# Basic markdown reply preview prototype

This is just a simple demo project to learn how to do useful things.

## Goals of this prototype

* Experiment with build simple web component framework.
* Work with as vanilla typescript setup as possible.
* Create a nice markdown preview box that might be used on a comment section on a website.

## Pre-requisites

It has been tested with the following node versions

```bash
$ node -v && npm -v
v13.5.0
6.13.4
```

## Installation

```bash
$ npm install
```

## Run example

```bash
$ npm run start
```
